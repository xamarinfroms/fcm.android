﻿using Android.App;
using Firebase.Iid;

namespace FCM.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseIIDService: FirebaseInstanceIdService
    {
        public override void OnTokenRefresh()
        {
            //由FCM取得Token,或Token有更新時
            var refreshedToken = FirebaseInstanceId.Instance.Token;

            //to do somethings...(ex:throw the token to server)
        }
    }
}