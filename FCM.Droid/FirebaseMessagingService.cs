﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Firebase.Messaging;

namespace FCM.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class FirebaseMessagingServiceTest: FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage message)
        {
            //取得推播訊息
            this.SendNotification(message.GetNotification());
        }

       

        /// <summary>
        /// 建立推播通知
        /// </summary>
        /// <param name="notification"></param>
        private void  SendNotification(RemoteMessage.Notification notification)
        {
            var notificationManager = NotificationManager.FromContext(this);
            var builder = new Notification.Builder(Application.Context);
            builder
                .SetContentTitle(notification.Title)
                .SetContentText(notification.Body)
                 .SetSmallIcon(Resource.Drawable.Icon)
                 .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
                .SetDefaults(NotificationDefaults.All);
            notificationManager.Notify(0, builder.Build());
        }

    }
}