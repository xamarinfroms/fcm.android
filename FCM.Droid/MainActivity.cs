﻿using Android.App;
using Android.Content;
using Android.Gms.Common;
using Android.OS;
using Android.Support.V4.App;
using Android.Widget;
using Firebase.Iid;
using Firebase.Messaging;
using System;

namespace FCM.Droid
{
    [Activity(Label = "FCM.Droid", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            //檢查裝置上的googleservice服務
            var lblState = this.FindViewById<TextView>(Resource.Id.lblState);

            int checkResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if(checkResult!= ConnectionResult.Success)
            {

                lblState.Text = GoogleApiAvailability.Instance.IsUserResolvableError(checkResult) ?
                                                GoogleApiAvailability.Instance.GetErrorString(checkResult) :
                                                "未偵測到Google Play Services ,無法使用";
                return;
            }
            lblState.Text = "Google Play Services 已偵測";


            this.FindViewById<Button>(Resource.Id.btnToken).Click += (sender, e) =>
            {
                //印出Token
                Console.WriteLine(FirebaseInstanceId.Instance.Token);
                lblState.Text = FirebaseInstanceId.Instance.Token;
            };

            this.FindViewById<Button>(Resource.Id.btnSubscribe).Click += (sender, e) =>
            {
                //訂閱主題為test的推播
                FirebaseMessaging.Instance.SubscribeToTopic("test");
            };
            
        }
    }
}

